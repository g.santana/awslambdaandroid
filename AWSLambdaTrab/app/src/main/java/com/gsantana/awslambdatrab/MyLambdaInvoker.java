package com.gsantana.awslambdatrab;

import com.amazonaws.mobileconnectors.lambdainvoker.LambdaFunction;

public interface MyLambdaInvoker {

    /** Invocação da função Lambda "AndroidBackendLambdaFunction". */
    @LambdaFunction(functionName = "aws-lambda-maps")
    String AndroidBackendLambdaFunction(RequestSender request);

}
