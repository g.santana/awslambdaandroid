package com.gsantana.awslambdatrab;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.amazonaws.mobileconnectors.lambdainvoker.*;
import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnSuccessListener;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, LocationListener {

    private GoogleMap mMap;
    private FusedLocationProviderClient fusedLocationClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        getPermissions();
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

    }

    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        /** Instância responsável por fazer as requisições de localização. */
        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        fusedLocationClient.getLastLocation().addOnSuccessListener(this, new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                invokeLambda(new LatLng(location.getLatitude(), location.getLongitude()));
            }
        });

        /** Ativação da localização do usuário no mapa. */
        mMap.setMyLocationEnabled(true);

        /**
         *  Requisição de atualização da posição do dispositivo a seguir.
         *  Argumentos: nome do network location provider, tempo (ms) mínimo entre requisições,
         *              distância (m) mínima entre requisições e listener (auto-referenciado).
        */
       // lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1, 1, this);
    }

    public void getPermissions(){
        if (ActivityCompat.checkSelfPermission(MapsActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(MapsActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MapsActivity.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            return;
        }else{
            return;
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        /** Obtenção das coordenadas do dispositivo (latitude, longitude). */
        LatLng pos = new LatLng(location.getLatitude(), location.getLongitude());

        /** Instância de um objeto p/ realizar a movimentação da câmera do mapa p/ posição atual. */
        CameraUpdate cam = CameraUpdateFactory.newLatLngZoom(pos, 10);

        /** Chamada que movimenta a câmera do mapa p/ posição atual. */
        mMap.animateCamera(cam);

        /** Invocação da função lambda na AWS. */
        invokeLambda(pos);

        /** */

    }

    /**
     *  Declaração obrigatória de métodos da interface LocationListener a seguir, cuja
     *  implementação não é obrigatória. Precisamos somente do método onLocationChanged().
     */

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) { }

    @Override
    public void onProviderEnabled(String provider) { }

    @Override
    public void onProviderDisabled(String provider) { }

    public void invokeLambda(LatLng pos){
        // Create an instance of CognitoCachingCredentialsProvider
        CognitoCachingCredentialsProvider credentialsProvider = new CognitoCachingCredentialsProvider(
                getApplicationContext(),
                "us-east-1:742dc518-49f5-4d3a-acd0-8f600d046915", // ID do grupo de identidades
                Regions.US_EAST_1 // Região
        );

        // Create LambdaInvokerFactory, to be used to instantiate the Lambda proxy.
        LambdaInvokerFactory factory = new LambdaInvokerFactory(this.getApplicationContext(),
                Regions.US_EAST_1, credentialsProvider);

        // Create the Lambda proxy object with a default Json data binder.
        // You can provide your own data binder by implementing
        // LambdaDataBinder.
        final MyLambdaInvoker myInvoker = factory.build(MyLambdaInvoker.class);

        RequestSender request = new RequestSender(pos.latitude, pos.longitude);
        // The Lambda function invocation results in a network call.
        // Make sure it is not called from the main thread.
        new AsyncTask<RequestSender, Void, String>() {
            @Override
            protected String doInBackground(RequestSender... params) {
                // invoke "echo" method. In case it fails, it will throw a
                // LambdaFunctionException.
                try {
                    return myInvoker.AndroidBackendLambdaFunction(params[0]);
                } catch (LambdaFunctionException lfe) {
                    Log.e("Tag", "Failed to invoke echo", lfe);
                    return null;
                }
            }

            @Override
            protected void onPostExecute(String result) {
                if (result == null) {
                    return;
                }

                // Do a toast
                Toast.makeText(MapsActivity.this, result, Toast.LENGTH_LONG).show();
            }
        }.execute(request);
    }


}
