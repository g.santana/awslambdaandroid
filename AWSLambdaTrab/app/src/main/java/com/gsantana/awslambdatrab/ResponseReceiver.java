package com.gsantana.awslambdatrab;

public class ResponseReceiver {
    String resposta;

    public String getResposta() {
        return resposta;
    }

    public ResponseReceiver(String resposta) {
        this.resposta = resposta;
    }
}
